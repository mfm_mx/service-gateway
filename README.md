# service-gateway
---

## <a id="index"></a>Index

* [**Description.**](#description)
* [**Configuration.**](#configuration)
* [**Packaging.**](#packaging)
* [**List of Services.**](#list-of-services)
* [**Changelog.**](#changelog)
* [**Additional Resources.**](#additional-resources)

## <a id="description"/></a>Description

**service-gateway** component is responsible of:

> - Dinamically find available services registered in Hashicorp's Consul.
> - Load balance service requests between available services


## <a id="configuration"/></a>Configuration


[Back to Index ^](#index))

## <a id="packaging"/></a>Packaging

In order to package this component, **mvn clean package** command must be run.

[Back to Index ^](#index)

## <a id="list-of-services"/></a>List of Services


[Back to list of Services ^](#list-of-services)

[Back to Index ^](#index)

## <a id="changelog"/></a>Changelog
| VERSION       | DESCRIPTION  |
|:-------------:|:-------------|
|1.0.0| Initial Version|
[Back to Index ^](#index)
