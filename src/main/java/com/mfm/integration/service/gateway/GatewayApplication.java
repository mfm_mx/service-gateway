package com.mfm.integration.service.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@EnableZuulProxy
@EnableAutoConfiguration
@EnableDiscoveryClient
@SpringBootApplication
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}
	
	@Bean
	public ZuulRequestLoggingFilter addRequestLoggingFilter() {
		return new ZuulRequestLoggingFilter();
	}

	@Bean
	public ZuulResponseLoggingFilter addResponseLoggingFilter() {
		return new ZuulResponseLoggingFilter();
	}
	
	@Bean
	public ZuulRelayTokenFilter addRelayTokenFilter() {
		return new ZuulRelayTokenFilter();
	}

}
