package com.mfm.integration.service.gateway;

import static com.netflix.zuul.context.RequestContext.getCurrentContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class ZuulRequestLoggingFilter extends ZuulFilter {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public Object run() {
		RequestContext context = getCurrentContext();

		String transientMessageId = context.getRequest().getHeader("MESSAGE_ID");
		boolean transientMessageIdReceived = true;

		if (transientMessageId == null || transientMessageId.isEmpty()) {
			transientMessageIdReceived = false;
			transientMessageId = UUID.randomUUID().toString();
			logger.info("MESSAGE_ID is NOT present on http headers... MESSAGE_ID created: " + transientMessageId);
		}
		
		final String messageId = transientMessageId;
		final boolean messageIdReceived = transientMessageIdReceived;
		
		MDC.put("MESSAGE_ID", messageId);
		logger.info("Processing message MESSAGE_ID: " + messageId);
		
		String jsonTransportHeaders = getTransportHeadersAsJson(getCurrentContext().getRequest());
		logger.info("Request URI: " + getCurrentContext().getRequest().getRequestURI() + ", Request Headers: " + jsonTransportHeaders);		
		
		context.setRequest(new HttpServletRequestWrapper(getCurrentContext().getRequest()) {

			@Override
			public String getHeader(String name) {
				if (!name.equalsIgnoreCase("MESSAGE_ID")) {
					return super.getHeader(name);
				}
				else if (name.equalsIgnoreCase("MESSAGE_ID") && messageIdReceived) {
					return super.getHeader(name);
				} else {
					return messageId;
				}
			}

			@Override
			public Enumeration<String> getHeaderNames() {
				if (messageIdReceived) {
					return super.getHeaderNames();
				} else {
					ArrayList<String> headerNames = Collections.list(super.getHeaderNames());
					headerNames.add("MESSAGE_ID");
					return Collections.enumeration(headerNames);
				}
			}

			@Override
			public Enumeration<String> getHeaders(String name) {
				if (!name.equalsIgnoreCase("MESSAGE_ID")) {
					return super.getHeaders(name);
				}else if (name.equalsIgnoreCase("MESSAGE_ID") && messageIdReceived) {
					return super.getHeaders(name);
				} else {
					return Collections.enumeration(Collections.singletonList(messageId));
				}
			}
		});
		return null;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public int filterOrder() {
		return 6;
	}

	@Override
	public String filterType() {
		return "pre";
	}

	private String getTransportHeadersAsJson(HttpServletRequest request) {
		Map<String, Object> headerMap = new HashMap<String, Object>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = (String) headerNames.nextElement();
			headerMap.put(headerName, request.getHeader(headerName));
		}
		String jsonTransportHeaders = JSONObject.toJSONString(headerMap);
		return jsonTransportHeaders;
	}
	
}
