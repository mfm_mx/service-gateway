package com.mfm.integration.service.gateway;

import static com.netflix.zuul.context.RequestContext.getCurrentContext;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.netflix.util.Pair;
import com.netflix.zuul.ZuulFilter;

public class ZuulResponseLoggingFilter extends ZuulFilter {

	Logger logger = LoggerFactory.getLogger(getClass());

	public static Predicate<Pair<String, String>> isMessageIdPresent() {
	    return p -> p.first().equals("MESSAGE_ID");
	}	
	
	@Override
	public Object run() {

		String messageId = MDC.get("MESSAGE_ID");		
		logger.info("Message processing ends MESSAGE_ID: " + messageId);
		
		HttpServletResponse res = getCurrentContext().getResponse();
		List<Pair<String,String>> messageIdFromResponse = 
				getCurrentContext().getZuulResponseHeaders().stream()
					.filter(isMessageIdPresent())
					.collect(Collectors.<Pair<String,String>>toList());
		if(!res.containsHeader("MESSAGE_ID") && 
				(messageIdFromResponse == null || messageIdFromResponse.isEmpty())) {
			res.addHeader("MESSAGE_ID", messageId);
		}

		MDC.clear();
		return null;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public int filterOrder() {
		return 999;
	}

	@Override
	public String filterType() {
		return "post";
	}
	
}
